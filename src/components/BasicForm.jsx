import { useState, useEffect } from "react";

function BasicForm() {

    const [value, setValue] = useState('');

    const [errorMessage, setErrorMessage] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        alert(`Le texte saisi est ${value}`);
    };

    useEffect(() => {
        if (!validate(value)) {
            setErrorMessage('Le champ ne peut être vide.');
        } else {
            setErrorMessage('');
        }
    }, [value]);

    function validate(input) {
        return input.length > 0;
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={value}
                onChange={(e) => setValue(e.target.value)}
            />
            {errorMessage && <div>{errorMessage}</div>}
            <button type="submit">Envoyer</button>
        </form>

    );
}

export default BasicForm;