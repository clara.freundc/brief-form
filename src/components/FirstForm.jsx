import React, { useState, useEffect } from "react";
import "./FirstForm.css";

function FirstForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    useEffect(() => {
        if (age && parseInt(age) < 18) {
            setErrorMessage('Vous devez avoir plus de 18 ans pour remplir ce formulaire.');
        } else {
            setErrorMessage('');
        }
    }, [age]);

    const handleSubmit = (e) => {
        e.preventDefault();
            console.log(firstName,lastName,age,email,password,errorMessage)
    };

    return (
        <form className="subscription" onSubmit={handleSubmit}>
            <label htmlFor="firstname">Entrez votre prénom :</label>
            <input
                id="firstname"
                type="text"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                placeholder="Votre prénom"
            />
            <label htmlFor="lastname">Entrez votre nom :</label>
            <input
                id="lastname"
                type="text"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                placeholder="Votre nom"
            />
            <label htmlFor="age">Entrez votre âge :</label>
            <input
                id="age"
                type="number"
                value={age}
                onChange={(e) => setAge(e.target.value)}
                placeholder="Votre âge"
                min="0"
                max="100"
            />
            {errorMessage && <div>{errorMessage}</div>}
            <label htmlFor="email-adress">Entrez votre adresse email :</label>
            <input
                id="email-adress"
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                placeholder="Votre adresse mail"
            />
            <label htmlFor="password">Entrez votre mot de passe :</label>
            <input
                id="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Votre mot de passe"
            />
            <button type='submit'>Envoyer</button>
        </form>
    );
}

export default FirstForm;
